# Shinobi Nightmare Data-Mine

This git contains tools to download and extract assets of the game [Shinobi Nightmare](https://sn.fg-games.co.jp/).

The ownership of the assets belongs to [gumi](https://gu3.co.jp).


## Scripts

### Requirements
```cmd
pip install UnityPy
```

### EXTRACT.py
* extracts all images from the downloaded raw files (in \files) to \assets.

### MERGER.py
* merges the alpha channel into the images in \files and saves the result in \images



