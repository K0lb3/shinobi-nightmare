import os
from PIL import Image

for root, dirs, files in os.walk("assets", topdown=False):
   for f in files:
       if f.endswith('_a.png'):
            try:
                fp = os.path.join(root, f[:-6])
                dfp = os.path.dirname(fp.replace('assets','images'))+'.png'
                os.makedirs(os.path.dirname(dfp), exist_ok=True)
                
                alpha_img = Image.open(os.path.join(root, f))
                if os.path.exists(fp+'.png'):
                    img = Image.open(fp+'.png')
                elif os.path.exists(fp+'_RGB.png'):
                    img = Image.open(fp+'_RGB.png')

                Image.merge('RGBA', (*img.split()[:3], *alpha_img.convert('L').split())).save(dfp)
            except FileNotFoundError:
                continue
            except ValueError:
                continue
            #channels=(*t.split()[:3],a.getchannel('R'))
		    #Image.merge("RGBA", channels).save(target[:-4]+'.png')