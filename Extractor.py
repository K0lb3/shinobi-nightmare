import os
from UnityPy import AssetsManager

dst = ''

def save_obj(obj, path, name=False):
    path = os.path.join(dst, *path.split('/'))
    
    if obj.type not in ['Texture2D','Sprite']:
        return
    
    data = obj.read()
    
    if name:
        path = os.path.join(path, data.name)
    
    os.makedirs(os.path.dirname(path), exist_ok=True)
    
    if obj.type == 'Sprite':
        if not path.endswith('.png'):
            path += '.png'
        data.image.save(path)
    elif obj.type == 'Texture2D':
        if not path.endswith('.png'):
            path += '.png'
        if not os.path.exists(path):
            data.image.save(path) 
    

for fp in os.listdir('files'):
    fp = os.path.join('files',fp)
    a = AssetsManager()
    a.load_file(fp)
    for asset in a.assets.values():
        len_container = len(asset.container)
        if len_container == 0:
            print(asset.name)
            continue
        elif len(asset.container) == 1:
            path, obj = list(asset.container.items())[0] 
            if obj.type in ['Sprite', 'Texture2D']:
                save_obj(obj, path)
            else:
                path = os.path.dirname(path)
                for obj in asset.objects.values():
                    save_obj(obj, path, True)
        else:
            for path, obj in asset.container.items():
                save_obj(obj, path)
            